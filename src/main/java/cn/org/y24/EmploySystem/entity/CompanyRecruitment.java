package cn.org.y24.EmploySystem.entity;

public class CompanyRecruitment {
    private int companyId;
    private String recruitmentId;
    private String releaseTime;

    public CompanyRecruitment(int companyId, String recruitmentId, String releaseTime) {
        this.companyId = companyId;
        this.recruitmentId = recruitmentId;
        this.releaseTime = releaseTime;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getRecruitmentId() {
        return recruitmentId;
    }

    public void setRecruitmentId(String recruitmentId) {
        this.recruitmentId = recruitmentId;
    }

    public String getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(String releaseTime) {
        this.releaseTime = releaseTime;
    }

    @Override
    public String toString() {
        return "CompanyRecruitment{" +
                "companyId=" + companyId +
                ", recruitmentId='" + recruitmentId + '\'' +
                ", releaseTime='" + releaseTime + '\'' +
                '}';
    }
}
