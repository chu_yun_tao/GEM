package cn.org.y24.EmploySystem.entity;

public class AdminUserAdditionalInfo {
    private int id;
    private String username;
    private String AuthenticationCode;

    public AdminUserAdditionalInfo(int id, String username, String authenticationCode) {
        this.id = id;
        this.username = username;
        AuthenticationCode = authenticationCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthenticationCode() {
        return AuthenticationCode;
    }

    public void setAuthenticationCode(String authenticationCode) {
        AuthenticationCode = authenticationCode;
    }

    @Override
    public String toString() {
        return "AdminUserAdditionalInfo{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", AuthenticationCode='" + AuthenticationCode + '\'' +
                '}';
    }
}
