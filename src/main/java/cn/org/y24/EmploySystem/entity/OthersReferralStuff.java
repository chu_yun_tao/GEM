package cn.org.y24.EmploySystem.entity;

import java.util.Date;

public class OthersReferralStuff {
    private int ID;
    private Date time;
    private String name;
    private String contractNo;
    private String graduateIdCardNo;

    public OthersReferralStuff(int ID, Date time, String name, String contractNo, String graduateIdCardNo) {
        this.ID = ID;
        this.time = time;
        this.name = name;
        this.contractNo = contractNo;
        this.graduateIdCardNo = graduateIdCardNo;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getGraduateIdCardNo() {
        return graduateIdCardNo;
    }

    public void setGraduateIdCardNo(String graduateIdCardNo) {
        this.graduateIdCardNo = graduateIdCardNo;
    }

    @Override
    public String toString() {
        return "OthersReferralStuff{" +
                "ID=" + ID +
                ", time=" + time +
                ", name='" + name + '\'' +
                ", contractNo='" + contractNo + '\'' +
                ", graduateIdCardNo='" + graduateIdCardNo + '\'' +
                '}';
    }
}
