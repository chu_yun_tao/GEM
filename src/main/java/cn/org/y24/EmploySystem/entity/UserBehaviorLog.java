package cn.org.y24.EmploySystem.entity;

import java.util.Date;

public class UserBehaviorLog {
    private int id;
    private String username;
    private Date time;

    public UserBehaviorLog(int id, String username, Date time) {
        this.id = id;
        this.username = username;
        this.time = time;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "UserBehaviorLog{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", time=" + time +
                '}';
    }
}
