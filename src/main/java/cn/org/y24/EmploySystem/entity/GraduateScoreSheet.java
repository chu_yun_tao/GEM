package cn.org.y24.EmploySystem.entity;

public class GraduateScoreSheet {
    private int id;
    private String school;
    private int graduateIdCardNo;
    private String subject;
    private float score;

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGraduateIdCardNo() {
        return graduateIdCardNo;
    }

    public void setGraduateIdCardNo(int graduateIdCardNo) {
        this.graduateIdCardNo = graduateIdCardNo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public GraduateScoreSheet(int id, String school, int graduateIdCardNo, String subject, float score) {
        this.id = id;
        this.school = school;
        this.graduateIdCardNo = graduateIdCardNo;
        this.subject = subject;
        this.score = score;
    }

    @Override
    public String toString() {
        return "GraduateScoreSheet{" +
                "id=" + id +
                ", school='" + school + '\'' +
                ", graduateIdCardNo='" + graduateIdCardNo + '\'' +
                ", subject='" + subject + '\'' +
                ", score=" + score +
                '}';
    }
}
