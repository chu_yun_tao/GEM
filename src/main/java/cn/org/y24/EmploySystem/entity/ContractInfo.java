package cn.org.y24.EmploySystem.entity;

import java.util.Date;

public class ContractInfo {
    private int graduateId;
    private int companyId;
    /// 职位名称
    private String name;
    private Date begin;
    private Date end;
    private String description;

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getGraduateId() {
        return graduateId;
    }

    public void setGraduateId(int graduateId) {
        this.graduateId = graduateId;
    }

    public ContractInfo(int graduateId, int companyId, String name, Date begin, Date end, String description) {
        this.graduateId = graduateId;
        this.companyId = companyId;
        this.name = name;
        this.begin = begin;
        this.end = end;
        this.description = description;
    }

    @Override
    public String toString() {
        return "ContractInfo{" +
                "graduateId=" + graduateId +
                ", companyId=" + companyId +
                ", name='" + name + '\'' +
                ", begin=" + begin +
                ", end=" + end +
                ", description='" + description + '\'' +
                '}';
    }
}
